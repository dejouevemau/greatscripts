#coding:utf-8
#mechanize範例, 來自http://stockrt.github.io/p/emulating-a-browser-in-python-with-mechanize/

import mechanize
import cookielib

################Create the Browser################
# Browser
br = mechanize.Browser()

# Cookie Jar
cj = cookielib.LWPCookieJar()
br.set_cookiejar(cj)

# Browser options
br.set_handle_equiv(True)
br.set_handle_gzip(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(False) #這句貌似是用來忽略robots.txt裏面的規則

# Follows refresh 0 but not hangs on refresh > 0
br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time = 1)

# Want debugging messages?
#br.set_debug_http(True)
#br.set_debug_redirects(True)
#br.set_debug_responses(True)

# User-Agent
ua = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11'
br.addheaders = [('User-Agent',ua),
    ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
    ('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.3'),
    ('Accept-Encoding', 'none'),
    ('Accept-Language', 'en-US,en;q=0.8'),
    ('Connection', 'keep-alive')]

# Set a proxy for http
# XXNet做代理是需要證書的, mechanize應該不能handle這麼高級的任務
#br.set_proxies({"http": "localhost:8087"})
# password
#br.add_proxy_password("name", "password")
# or
#br.set_proxies({"http", "name:password@site.com:8087"})

################Search on some site#################
# Open a site
# If the site need a password(http basic auth). Authentication error is 410
#br.add_password('http://xxx.com', 'username', 'password')
r = br.open('http://bing.com')
html = r.read()

# Show the source
#print html
# or
#print '-' * 30
#print br.response().read()

# Show the html title
print br.title()

# Show the response headers
#print r.info()
# or
#print '-' * 30
#print br.response().info 這個跟r.info()的結果並不一樣？

# Show the available forms
for f in br.forms():
    print f #會列出所有的表及其內容，對於bing.com，只有一個search表

# Select the first (index zero) form
br.select_form(nr = 0) 
#選擇一個表之後，會把這個表的內容作爲一個字典，其中的(q=)就是說這個區域
#在字典中的key是q，一般文本輸入框會是類似於<TextControl(q=)>這種形式。
#另外還有一些內容是readonly的

# Let's search
print '#' * 20
print 'Entering search result:'
br.form['q'] = 'weekend codes' #字典中搜索詞對應的key爲q
br.submit()
#print br.response().read()
print br.title()

#################Click some link on the search result##############
# Looking at some results in link format
for l in br.links(url_regex = 'stockrt'):
    print '-' * 20
    print l

# Testing presence of link
br.find_link(text='Weekend Codes - stockrt.github.io')
#如果沒有對應鏈接，就會返回錯誤並中止程序

# Actually clicking the link
print '#' * 20
print 'Entering the page that matches:'
req = br.click_link(text='Weekend Codes - stockrt.github.io')
#實際上就是獲得了對應文本的鏈接
br.open(req)
#print br.response().read()
print br.geturl() #獲得當前網址

#################Other Usage#################
# Managing browsing history
print '#' * 20
print 'Getting back to the previous page:'
br.back()
print br.geturl()
print br.title()

# Download
#f = br.retrieve('http:..../name.gif')[0]
#print f
#fh = open(f)